# Rain Gauge With Temperature Humidity and UV Sensor

The Rain Gauge Station – Model 3424 generates real-time data to help in flood warning, reservoir management, and any other application that calls for timely rainfall information.When there is a chance of flooding, the information from the Rain Gauges helps emergency management experts make decisions that can save both lives and property. These stations are typically used in weather, meteorology, and mesonet applications. The Raingauge station developed using Davis rainguage with a battery-solar powered.

Overexposure to solar ultraviolet (UV) radiation is a risk for public health. Therefore, it is important to provide information to the public about the level of solar UV. The UV-Index (UVI) is the relevant quantity, expressing the erythemally weighted irradiance to a horizontal plane on a simple scale. As solar UV irradiance is strongly variable in time and space, measurements within a network provide the best source of information, provided they can be made available rapidly.

SHT20 I2C temperature & humidity sensor is equipped with waterproof probe. It comes with the 4C CMOSens® SHT20 temperature & humidity sensor chip and the probe has gone through dual waterproof protection test.
The SHT20 I2C temperature & humidity sensor adopt Sensirion techniques. Except the humidity sensor of capacitive type and the temperature sensor of band gap , SHT20 contains an amplifier, A/D converter, OTP memory and a digital processing unit. Compared with early SHT1x series and SHT7x series, SHT20 shows a strong reliability and long-term stability. It can measure surrounding environment temperature and relative air humidity precisely.

# List of Parameters recorded

- Rain measurement
- UV Index measurement
- Temperature and Humidity measurement
- Every 15 minutes rainfall and UV Index data
- Daily Rainfall data(Transmits every day 8.00 AM)

LoRaWAN based Automatic Rain Gauge Station is a system which measure the Total rainfall and transmits the data at regular intervals.
List of Parameters recorded
The application code is written on top of the I-cube-lrwan package by STM32.

# Prerequisites
    
- STM32 Cube IDE [Tested]
- [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
- Solar Charge Controller
- Solar Panel (3V, 6V voc)
- [Rain guage Davis](https://www.davisinstruments.com/products/aerocone-rain-collector-with-flat-base-for-vantage-pro2?_pos=3&_sid=2be3af584&_ss=r)
- [UV Sensor Davis](https://www.davisinstruments.com/products/uv-sensor)
- SHT20 I2C Temperature Humidity Sensor
## Getting started
 
- Make sure that you have a [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0) or any STM32 Board.
- Connect the components as shown in the [Wiring Diagram](https://gitlab.com/SruthyS/rainguage-with-uv-sensor/-/blob/main/Hardware/Wiring%20Diagram/uv_wiring%20diagram.pdf)
- Install STM32 Cube IDE.
- Select board : B-L072Z-LRWAN1
- For programming connect C1_dev_v1.0 through B-L072Z-LRWAN1 [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_conection_STM32board.pdf).
- For programming connect C1_dev_v1.0 through STM32 programmer Module [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_connection_STMProgrammer.pdf).
- If using ABP(Activation by Personalisation) method, Change the address/keys of the device address, network session key, application session key.
- If using OTAA (Over the Air Activation) method, Change the address/keys of the DUI (device EUI), application key.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details
