/*
 * raingauge_station.h
 *
 *  Created on: 16-May-2022
 *      Author: arun
 */

#ifndef WEATHER_STATION_RAINGAUGE_STATION_H_
#define WEATHER_STATION_RAINGAUGE_STATION_H_
#ifdef __cplusplus
extern "C" {
#endif

#define BATT_POWER    				1
#define ENABLE_SHT20
#define BME280_POWER    3

#define BME280_INIT_SUCCESS   0
#define BME280_INIT_ERROR     1
#define BME280_READ_FAIL      1
#define BME280_READ_SUCCESS   0

#define DOWNLINK_RAINPORT        	5

#define RAIN_MEMORY_ADD				0x08080008

typedef struct {


	uint16_t rainfall;

    uint16_t batteryLevel;

    uint16_t totalrainfall;

    uint16_t downlinkRainfall;

    uint16_t uv;

    int16_t temperature;

    uint16_t humidity;

}rainfallData_t;

uint16_t readBatteryLevel(void);

void rainGaugeInterruptEnable();
void rainGaugeTips();
uint16_t getRainfall();
uint16_t getTotalRainfall(uint8_t downlinkReceived);

void enable(uint8_t);
void disable(uint8_t);
void raingaugeStationInit();
void readRaingaugeStationParameters(rainfallData_t *sensor_data);
void raingaugeStationGPIO_Init();
void SHT20Read(rainfallData_t *sensor_data);
void sht20init();


#ifdef __cplusplus
}
#endif

#endif /* WEATHER_STATION_RAINGAUGE_STATION_H_ */
